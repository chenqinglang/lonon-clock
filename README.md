# lononClock
```
该项目是一个时钟，使用 esp32s2 主控，支持 WIFI 联网 http 请求同步时间，支持单声道 ES8311 DAC音频解码播放闹钟音乐。

支持乐鑫官方应用 “Espressif Esptouch” 进行网络配置。
```
<img src=./doc/image/image.jpg width=80% />

<img src=./doc/image/image(2).png width=80% />
<img src=./doc/image/image(3).png width=80% />
<img src=./doc/image/image(4).png width=80% />
<img src=./doc/image/image(5).png width=80% />

## 注意事项
```
需要进入 idf.py menuconfig 调整 RTOS 时间片，100HZ 修改为 200HZ 以配合 74HC595 5ms 速度扫描显示时间。
```







