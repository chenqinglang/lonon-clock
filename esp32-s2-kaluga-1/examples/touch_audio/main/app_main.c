// Copyright 2019 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_heap_caps.h"
#include "esp_spiffs.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "lwip/apps/sntp.h"
#include "i2c_bus.h"
#include "driver/rmt.h"
#include "led_strip.h"
#include "touch.h"
#include "audio.h"
#include "es8311.h"
#include "board.h"
#include "include.h"
#include "audio.h"
#include "storage.h"
#include "app_main.h"

static const char *TAG = "main";

extern xQueueHandle digital_queue;
extern xQueueHandle wifi_evt_queue;
extern xQueueHandle timesmap_queue;
extern xQueueHandle play_queue;

led_strip_t *strip;

esp_err_t spiffs_init(void)
{
    esp_err_t ret = ESP_OK;
    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
        .base_path = "/spiffs",
        .partition_label = NULL,
        .max_files = 5,
        .format_if_mount_failed = true};

    /*!< Use settings defined above to initialize and mount SPIFFS filesystem. */
    /*!< Note: esp_vfs_spiffs_register is an all-in-one convenience function. */
    ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK)
    {
        if (ret == ESP_FAIL)
        {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        }
        else if (ret == ESP_ERR_NOT_FOUND)
        {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        }
        else
        {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }

        return ret;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);

    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    }
    else
    {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

    /*!< Open renamed file for reading */
    ESP_LOGI(TAG, "Reading file");
    FILE *f = fopen("/spiffs/spiffs.txt", "r");

    if (f == NULL)
    {
        ESP_LOGE(TAG, "Failed to open file for reading");
        return ESP_FAIL;
    }

    char line[64];
    fgets(line, sizeof(line), f);
    fclose(f);
    /*!< strip newline */
    char *pos = strchr(line, '\n');

    if (pos)
    {
        *pos = '\0';
    }

    ESP_LOGI(TAG, "Read from file: '%s'", line);

    return ESP_OK;
}

void app_main()
{
    unsigned int timesmap;
    unsigned int mp3_index = 0;
    uint32_t wifi_sta_fig;
    unsigned char digital[4];
    memset(digital, 0, sizeof(digital));

    /*!< Print basic information */
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    /*!< Initialize NVS */
    esp_err_t ret = nvs_flash_init();

    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    storage_init();
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(spiffs_init());
    ESP_ERROR_CHECK(i2c_bus_init());
    lonon_595_gpio_init();

    digital_queue = xQueueCreate(1, sizeof(digital));
    wifi_evt_queue = xQueueCreate(1, sizeof(wifi_sta_fig));
    timesmap_queue = xQueueCreate(1, sizeof(timesmap));
    play_queue = xQueueCreate(1, sizeof(audio_handle_t));

    xQueueSendFromISR(digital_queue, &digital, NULL);
    audio_init(strip);

    touch_init();

    vTaskDelay(1000 / portTICK_RATE_MS);
    vTaskDelay(1000 / portTICK_RATE_MS);
    vTaskDelay(1000 / portTICK_RATE_MS);

    wifi_ssid_info_t wifi_ssid_info;
    if (get_wifi_info(&wifi_ssid_info.ssid, &wifi_ssid_info.pwd) == ESP_OK)
        lonon_wifi_init_sta(&wifi_ssid_info.ssid, &wifi_ssid_info.pwd);
    else
        lonon_wifi_init_sta("ABC", "12345678");

    xTaskCreate(gpio_595_example, "gpio_595_example", 2048, NULL, 6, NULL);
    xTaskCreate(http_get_task, "http_get_task", 4096, NULL, 5, NULL);
    ESP_LOGI(TAG, "[APP] end..");
}
