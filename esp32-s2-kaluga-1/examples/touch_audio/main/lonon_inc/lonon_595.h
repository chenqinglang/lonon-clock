#ifndef _lonon_595_h_
#define _lonon_595_h_


#define DATA_595                37
#define SET_595                 36
#define CLK_595                 35
#define RES_595                 34
#define SEG_595                 33
#define SEG1                    26
#define SEG2                    21
#define SEG3                    20
#define SEG4                    19
#define GPIO_OUTPUT_PIN_SEL     ((1ULL<<DATA_595) | (1ULL<<SET_595) | (1ULL<<CLK_595) | (1ULL<<RES_595) | (1ULL<<SEG_595) | (1ULL<<SEG1) | (1ULL<<SEG2) | (1ULL<<SEG3) | (1ULL<<SEG4))
#define DELAY_TIME              5

void gpio_595_example(void* arg);
void lonon_595_gpio_init(void);

#endif