#ifndef _lonon_wifi_sta_h_
#define _lonon_wifi_sta_h_

/*

    函数功能：连接wifi站点
    函数名称：lonon_wifi_init_sta
    输入：*ssid             wifi名称
    输入：*pass             wifi密码

*/
void lonon_wifi_init_sta(char *ssid,char *pass);

#endif
