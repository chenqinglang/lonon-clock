#ifndef _include_h_
#define _include_h_

/*

    c

*/
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <sys/param.h>
/*

    rtos

*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
/*

    esp

*/
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_http_server.h"

/*

    lwip

*/


/*

    protocol

*/
#include "protocol_examples_common.h"
#include "tcpip_adapter.h"

/*

    cjson

*/
#include "cJSON.h"
/*

    drive

*/
#include "driver/gpio.h"
/*

    storage

*/
#include "nvs_flash.h"
#include "nvs.h"
/*

    lonon

*/
#include "lonon_wifi_sta.h"
#include "http_request.h"
#include "lonon_595.h"
#include "app_main.h"

#endif



