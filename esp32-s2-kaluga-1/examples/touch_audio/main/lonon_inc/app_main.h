#ifndef _app_main_h_
#define _app_main_h_

#include "stdint.h"

typedef struct
{
    uint8_t ssid[64];
    uint32_t pwd[64];
} wifi_ssid_info_t;

#endif
