#include "include.h"

xQueueHandle digital_queue = NULL;
xQueueHandle wifi_evt_queue = NULL;
xQueueHandle timesmap_queue = NULL;

extern xQueueHandle play_queue;

unsigned char fseg[] = {0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90};

static const char *TAG = "hello";

void set_595(unsigned char data)
{
    unsigned char i;
    for (i = 8; i >= 1; i--)
    {
        if (data & 0x80)
            gpio_set_level(DATA_595, 1);
        else
            gpio_set_level(DATA_595, 0);

        data <<= 1;
        gpio_set_level(CLK_595, 0);
        gpio_set_level(CLK_595, 1);
    }
}

enum
{
    AUDIO_STOP = 0,
    AUDIO_PLAY,
    AUDIO_NEXT,
    AUDIO_LAST
};

typedef struct
{
    uint32_t handle; // 0 = AUDIO_STOP 1 = AUDIO_PLAY 2 = AUDIO_NEXT 3 = AUDIO_LAST
    uint32_t list;   // 播放内容序号
} audio_handle_t;

void gpio_595_example(void *arg)
{
    unsigned int timesmap;
    unsigned char digital[4];
    unsigned char twinkle = 0;
    unsigned int twinkle_count = 0;
    unsigned int sec_count = 0;
    unsigned int digital_value = 0;

    unsigned int backup_fig = 0;

    memset(digital, 0, sizeof(digital));

    while (1)
    {
        if (xQueueReceive(timesmap_queue, &timesmap, 0))
        {
            printf("gpio_595_example: timesmap = %u\n", timesmap);

            timesmap += 60 * 60 * 8;
            int hour = (((timesmap % (60 * 60 * 24)) / 60) / 60);
            int min = (((timesmap % (60 * 60 * 24)) / 60) % 60);

            digital[0] = hour / 10;
            digital[1] = hour % 10;
            digital[2] = min / 10;
            digital[3] = min % 10;
        }

        set_595(fseg[digital[0]]);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        gpio_set_level(SET_595, 0);
        gpio_set_level(SET_595, 1);
        gpio_set_level(SEG1, 0);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        vTaskDelay(DELAY_TIME / portTICK_RATE_MS);

        set_595(fseg[digital[1]]);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        gpio_set_level(SET_595, 0);
        gpio_set_level(SET_595, 1);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 0);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        vTaskDelay(DELAY_TIME / portTICK_RATE_MS);

        set_595(fseg[digital[2]]);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        gpio_set_level(SET_595, 0);
        gpio_set_level(SET_595, 1);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 0);
        gpio_set_level(SEG4, 1);
        vTaskDelay(DELAY_TIME / portTICK_RATE_MS);

        set_595(fseg[digital[3]]);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 1);
        gpio_set_level(SET_595, 0);
        gpio_set_level(SET_595, 1);
        gpio_set_level(SEG1, 1);
        gpio_set_level(SEG2, 1);
        gpio_set_level(SEG3, 1);
        gpio_set_level(SEG4, 0);
        vTaskDelay(DELAY_TIME / portTICK_RATE_MS);

        sec_count++;
        if (sec_count >= ((1000 * 1) / (DELAY_TIME * 4)))
        {
            sec_count = 0;

            timesmap++;
            int hour = (((timesmap % (60 * 60 * 24)) / 60) / 60);
            int min = (((timesmap % (60 * 60 * 24)) / 60) % 60);

            digital[0] = hour / 10;
            digital[1] = hour % 10;
            digital[2] = min / 10;
            digital[3] = min % 10;

            /*
                    定时播放
            */
            if (
                (digital[0] == 1 &&
                 digital[1] == 7 &&
                 digital[2] == 4 &&
                 digital[3] == 0) ||
                (digital[0] == 0 &&
                 digital[1] == 8 &&
                 digital[2] == 1 &&
                 digital[3] == 8) ||
                (digital[0] == 1 &&
                 digital[1] == 2 &&
                 digital[2] == 0 &&
                 digital[3] == 0))
            {
                if (backup_fig == 0)
                {
                    backup_fig = 1;
                    audio_handle_t audio_handle;
                    audio_handle.handle = AUDIO_PLAY;

                    xQueueSendFromISR(play_queue, &audio_handle, NULL);
                }
            }
            else
                backup_fig = 0;
        }

        twinkle_count++;
        if (twinkle_count > (700 / (DELAY_TIME * 4)))
        {
            twinkle_count = 0;
            if (twinkle)
            {
                twinkle = 0;
                gpio_set_level(SEG_595, 1);
            }
            else
            {
                twinkle = 1;
                gpio_set_level(SEG_595, 0);
            }
        }
    }
}

void lonon_595_gpio_init()
{
    gpio_config_t io_conf;
    // disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    // set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    // bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    // disable pull-down mode
    io_conf.pull_down_en = 0;
    // disable pull-up mode
    io_conf.pull_up_en = 0;
    // configure GPIO with the given settings
    gpio_config(&io_conf);

    gpio_set_level(SEG1, 0);
    gpio_set_level(SEG2, 0);
    gpio_set_level(SEG3, 0);
    gpio_set_level(SEG4, 0);

    gpio_set_level(RES_595, 0);
    vTaskDelay(10 / portTICK_RATE_MS);
    gpio_set_level(RES_595, 1);
}
