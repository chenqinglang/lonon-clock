/* HTTP GET Example using plain POSIX sockets

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "cJSON.h"

/* Constants that aren't configurable in menuconfig */
#define WEB_SERVER "quan.suning.com"
#define WEB_PORT "80"
#define WEB_PATH "/getSysTime.do"

static const char *TAG = "example";

static const char *json_TAG = "json";

static const char *REQUEST = "GET " WEB_PATH " HTTP/1.0\r\n"
                             "Host: " WEB_SERVER ":" WEB_PORT "\r\n"
                             "User-Agent: esp-idf/1.0 esp32\r\n"
                             "\r\n";

void http_get_task(void *pvParameters)
{
    const struct addrinfo hints = {
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM,
    };
    struct addrinfo *res;
    struct in_addr *addr;
    int s, r;
    char recv_buf[512];

    while (1)
    {
        int err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);

        if (err != 0 || res == NULL)
        {
            ESP_LOGE(TAG, "DNS lookup failed err=%d res=%p", err, res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        /* Code to print the resolved IP.

           Note: inet_ntoa is non-reentrant, look at ipaddr_ntoa_r for "real" code */
        addr = &((struct sockaddr_in *)res->ai_addr)->sin_addr;
        ESP_LOGI(TAG, "DNS lookup succeeded. IP=%s", inet_ntoa(*addr));

        s = socket(res->ai_family, res->ai_socktype, 0);
        if (s < 0)
        {
            ESP_LOGE(TAG, "... Failed to allocate socket.");
            freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... allocated socket");

        if (connect(s, res->ai_addr, res->ai_addrlen) != 0)
        {
            ESP_LOGE(TAG, "... socket connect failed errno=%d", errno);
            close(s);
            freeaddrinfo(res);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }

        ESP_LOGI(TAG, "... connected");
        freeaddrinfo(res);

        if (write(s, REQUEST, strlen(REQUEST)) < 0)
        {
            ESP_LOGE(TAG, "... socket send failed");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... socket send success");

        struct timeval receiving_timeout;
        receiving_timeout.tv_sec = 5;
        receiving_timeout.tv_usec = 0;
        if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout,
                       sizeof(receiving_timeout)) < 0)
        {
            ESP_LOGE(TAG, "... failed to set socket receiving timeout");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... set socket receiving timeout success");

        /* Read HTTP response */
        do
        {
            bzero(recv_buf, sizeof(recv_buf));
            r = read(s, recv_buf, sizeof(recv_buf) - 1);
            for (int i = 0; i < r; i++)
            {
                putchar(recv_buf[i]);
            }

            char *json_startp;
            json_startp = strstr(recv_buf, "{");

            if (json_startp != NULL)
            {
                ESP_LOGI(json_TAG, "find json head succ!!!");
                cJSON *json, *json_key;
                json = cJSON_Parse(json_startp);

                if (json != NULL)
                {
                    ESP_LOGI(json_TAG, "find json succ!!!");

                    json_key = cJSON_GetObjectItem(json, "sysTime1");

                    if (json_key != NULL)
                    {
                        // 20241018145841

                        ESP_LOGI(json_TAG, "json parse ok");
                        char sec_string[32];
                        memset(sec_string, 0, sizeof(sec_string));
                        strncpy(sec_string, json_key->valuestring, 14);
                        ESP_LOGI(json_TAG, "date time = %s", sec_string);

                        uint32_t year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
                        year = ((sec_string[0] - 0x30) * 1000) + ((sec_string[1] - 0x30) * 100) + ((sec_string[2] - 0x30) * 10) + ((sec_string[3] - 0x30) * 1);
                        mon = ((sec_string[4] - 0x30) * 10) + ((sec_string[5] - 0x30) * 1);
                        day = ((sec_string[6] - 0x30) * 10) + ((sec_string[7] - 0x30) * 1);
                        hour = ((sec_string[8] - 0x30) * 10) + ((sec_string[9] - 0x30) * 1);
                        min = ((sec_string[10] - 0x30) * 10) + ((sec_string[11] - 0x30) * 1);
                        sec = ((sec_string[12] - 0x30) * 10) + ((sec_string[13] - 0x30) * 1);

                        struct tm sourcedate1;

                        sourcedate1.tm_sec = sec;
                        sourcedate1.tm_min = min;
                        sourcedate1.tm_hour = hour;
                        sourcedate1.tm_mday = day;
                        sourcedate1.tm_mon = mon - 1;
                        sourcedate1.tm_year = year - 1900;
                        uint32_t t = mktime(&sourcedate1);

                        ESP_LOGI(json_TAG, "t = %d", t);

                        int timesmap = t - (60 * 60 * 8);
                        extern xQueueHandle timesmap_queue;
                        xQueueSendFromISR(timesmap_queue, &timesmap, NULL);
                        printf("http_get_task: timesmap = %d\n", timesmap);
                        ESP_LOGI(TAG, "... done reading from socket. Last read return=%d errno=%d.", r, errno);
                        cJSON_Delete(json);
                        goto en;
                    }
                    cJSON_Delete(json);
                }
            }
        } while (r > 0);

    en:

        ESP_LOGI(TAG, "... done reading from socket. Last read return=%d errno=%d.", r, errno);
        close(s);
        for (int countdown = 60 * 60; countdown >= 0; countdown--)
        {
            // ESP_LOGI(TAG, "%d... ", countdown);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
        ESP_LOGI(TAG, "Starting again!");
    }
}
